<?php
require __DIR__ . '/vendor/autoload.php';
require get_template_directory() . '/inc/template-tags.php';
require get_template_directory() . '/inc/template-functions.php';

use Werk\Init;

add_action('wp_body_open', 'show_template_files');

function show_template_files(){
    global $template;
    echo "<div class='template'>". basename($template) ."</div>";
}

new Init();