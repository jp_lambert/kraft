<?php
namespace Werk;
class Enqueues {
    public function __construct() {
        add_action( 'wp_enqueue_scripts', [ $this, 'scripts'] ); // [] == array()
    }

    public function scripts() {
        wp_enqueue_style( '_kraft-style', get_stylesheet_uri(), array(), _KRAFT_VERSION );
        wp_style_add_data( '_kraft-style', 'rtl', 'replace' );
    
        wp_enqueue_script( '_kraft-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _KRAFT_VERSION, true );
        wp_enqueue_script( '_kraft-custom', get_template_directory_uri() . '/js/custom.js', array(), _KRAFT_VERSION, true );
        wp_deregister_script( 'jquery' );
        wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.5.1.min.js", array(), '3.5.1', false );
    }

}
new Enqueues();