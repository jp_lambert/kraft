<?php 
namespace Werk;
use Werk\Enqueues;
use Werk\Domain\Tasks;
use Werk\Domain\Projets;
use Werk\Domain\Stories;

class Init
{
    public function __construct() {
        $this->initialize_kraft_version();
        add_action( 'init', [ $this, 'create_posttypes'] );
        add_action( 'widgets_init',  [ $this,'create_widgets'] );
        add_action( 'after_setup_theme', [ $this,'kraft_setup'] );
        new Enqueues();
    }

    public function create_posttypes(){
        // new Projets();
        // new Stories();
        // new Tasks();
    }
    
    public function initialize_kraft_version(){
        if ( ! defined( '_KRAFT_VERSION' ) ) {
            define( '_KRAFT_VERSION', '0.0.1' );
        }
    }

    public function kraft_setup(){
        load_theme_textdomain( '_kraft', get_template_directory() . '/languages' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
        add_theme_support( 'post-thumbnails' );
        add_theme_support( 'woocommerce' );
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Header menu', '_kraft' ),
				'menu-3' => esc_html__( 'Useless menu', '_kraft' ),
				'menu-2' => esc_html__( 'Footer menu', '_kraft' ),
			)
		);
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);	
    }

    public function create_widgets(){
        register_sidebar(
            array(
                'name'          => esc_html__( 'Sidebar', '_kraft' ),
                'id'            => 'sidebar-1',
                'description'   => esc_html__( 'Add widgets here.', '_kraft' ),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            )
        );
    }
    
}
