<?php
$stories = get_field('histoires_liees');
if ($stories) {
?>
    <section>
        <?php
        foreach ($stories as $post) {
            setup_postdata($post); ?>
            <header><h3><?php the_title(); ?></h3></header>
            <?php get_template_part("template-parts/tasks", "list"); ?>
        <?php }
        wp_reset_postdata(); ?>
    </article>
<?php }
