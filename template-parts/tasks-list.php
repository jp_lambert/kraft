<?php
$tasks = get_field('taches_liee');

if ($tasks) {
    foreach ($tasks as $post) {
        setup_postdata($post); 
        $priorite = get_field('priorite');
        $user = get_field('assign');
        $iter = 0;

        ?>
        <article>
            <header>
                <h5 class="item-<?php echo $iter; ?> priorite-<?php echo $priorite; ?>"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                <?php if ($user){?>
                <ul>
                    <li><?php echo $user["user_firstname"]; ?></li>
                    <li><?php echo $user["user_lastname"];  ?></li>
                </ul>
                <?php } ?>
            </header>
        </article>
        <?php
        $iter++;
        wp_reset_postdata();
    }
} ?>