<?php 
get_header();
$user = get_field('assign');
$priorite = get_field('priorite');
?>
<section>
    <header><h2 class="priorite-<?php echo $priorite; ?>"><?php the_title(); ?></h2></header>
    <article>
        <p><?php the_content();?></p>
        <h3>Assigné à:</h3>
        <ul>
            <li>Prénom:<?php echo $user["user_firstname"]; ?></li>
            <li>Nom : <?php echo $user["user_lastname"];  ?></li>
        </ul>
        <form action="/" method="post">
            <textarea name="comment" class="comment" cols="30" rows="10" placeholder="comment">
            
            </textarea>
        </form>
    </article>
</section>
<?php 
get_footer();
?>