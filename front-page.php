<?php

use Werk\Domain\Projets;

get_header();

?>
<main id="projets" class="site-main">
    <?php
    $projets = Projets::get_all_projects();

    if ($projets->have_posts()) {
        while ($projets->have_posts()) {
            $projets->the_post(); ?>
            <br> <a href="<?php echo get_the_permalink(); ?>"><?php echo get_the_title(); ?></a>

    <?php
        $termes = get_the_terms(get_the_ID(), 'project_type');
        }
    } ?>
</main>

<?php



$terms = get_terms( array(
    'taxonomy' => 'project_type',
    'hide_empty' => false,
) );


foreach($terms as $term){
    echo "nom $term->name";
}

get_footer();
