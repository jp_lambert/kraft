<?php 
get_header();
$stories = get_field('histoires_liees');

if( $stories ){ 
    foreach( $stories as $post ){
        setup_postdata($post); ?>
    <h1>
        <?php the_title(); ?>
    </h1>
    <?php get_template_part("template-parts/tasks","list");
     } 

    wp_reset_postdata(); ?>
<?php } 
get_footer();
?>