$(function(){
    var articleContent = $("article h5").next("ul");
    articleContent.hide();
    
    $("h5").on("click", function(){
        $("h5").next().hide();
        $(this).next().show();
    });
});