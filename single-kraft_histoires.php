<?php 
get_header();
$tasks = get_field('taches_liee');

if( $tasks ){ 
    foreach( $tasks as $post ){
        setup_postdata($post);
        get_template_part("template-parts/tasks","list");
        wp_reset_postdata(); 
    }
} 
get_footer();
?>